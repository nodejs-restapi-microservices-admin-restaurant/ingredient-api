# Documentation
* [Ingredient API](#ingredient-api)

## Ingredient API
### GET /ingredients
Returns all Ingredient instance.

Example response:
```json
[
    {
        "price": 100,
        "_id": "5dd098570cdb777077a0bec3",
        "title": "Японский рис",
        "restInStock": 500,
        "description": "Круглозерный японский рис, более твердый, чем другие круглозерные сорта.",
        "__v": 0
    }
]
```

### POST /ingredients
Adds to Ingredient instance in a database.

|Param|Type|
|--|--|
|title|`string`|
|description|`string`|
|restInStock|`number`|
|price|`number`|

Example request:
```json
{
    "price": 100,
    "title": "Японский рис",
    "restInStock": 500,
    "description": "Круглозерный японский рис, более твердый, чем другие круглозерные сорта."
}
```
Example response:
```json
{
    "price": 100,
    "_id": "5dd098570cdb777077a0bec3",
    "title": "Японский рис",
    "restInStock": 500,
    "description": "Круглозерный японский рис, более твердый, чем другие круглозерные сорта.",
    "__v": 0
}
```

### GET /ingredients/:id
Returns a Ingredient instance by id.

Example response:
```json
{
    "price": 100,
    "_id": "5dd098570cdb777077a0bec3",
    "title": "Японский рис",
    "restInStock": 500,
    "description": "Круглозерный японский рис, более твердый, чем другие круглозерные сорта.",
    "__v": 0
}
```
### PUT /ingredients/:id
Updates to Ingredient instance in a database.

|Param|Type|
|--|--|
|title|`string`|
|description|`string`|
|restInStock|`number`|
|price|`number`|

Example request:
```json
{
    "price": 1002,
    "title": "Японский рис",
    "restInStock": 500,
    "description": "Круглозерный японский рис, более твердый, чем другие круглозерные сорта."
}
```
Example response:
```json
{
    "message": "Successfully updated!"
}
```
### DELETE /ingredients/:id
Delete a Ingredient instance by id.

Example response:
```json
{
    "message": "Successfully deleted!"
}
```
