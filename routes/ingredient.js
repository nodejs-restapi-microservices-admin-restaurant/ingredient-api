const router = require('express').Router();
const DbWorker = require('../services/db-worker');
const withAuth = require('../middleware/withAuth');
const accessAdmin = require('../middleware/accessAdmin');

router.route('/')
    .get(withAuth, async function (req, res) {
        try {
            const ingredients = await DbWorker.getAll();
            await res.status(200).json(ingredients);
        } catch (err) {
            return res.status(500).send(err);
        }
    })
    .post(withAuth, accessAdmin, async function (req, res) {
        try {
            const newData = await DbWorker.create(req.body);
            await res.status(201).json(newData);
        } catch (err) {
            return res.status(500).send(err);
        }
    });

router.route('/:ingredientId')
    .get(withAuth, async function (req, res) {
        try {
            const ingredient = await DbWorker.getOneById(req.params.ingredientId);
            await res.status(200).json(ingredient);
        } catch (err) {
            return res.status(500).send(err);
        }
    })
    .put(withAuth, accessAdmin, async function (req, res) {
        try {
            const newData = await DbWorker.update(req.params.ingredientId, req.body);
            await res.status(200).json(newData);
        } catch (err) {
            return res.status(500).send(err);
        }
    })
    .delete(withAuth, accessAdmin, async function (req, res) {
        try {
            const delData = await DbWorker.delete(req.params.ingredientId);
            await res.status(200).json(delData);
        } catch (err) {
            return res.status(500).send(err);
        }
    });

module.exports = router;

